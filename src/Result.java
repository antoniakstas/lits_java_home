public class Result extends Duskruminant {

	public Result(int a, int b, int c) {

		super(a, b, c);
	}

	public float calculateDuskruminant() {

		return super.calculateDuskruminant();
	}

	public double calculateResult(){

		double Res = 0;

		if (calculateDuskruminant()>0){
			double x1, x2;

			x1=(-getB() + Math.sqrt(calculateDuskruminant()))/(2*getA());
			x2=(-getB() - Math.sqrt(calculateDuskruminant()))/(2*getA());

			Res = x1;

		}else if (calculateDuskruminant() == 0){
			double x;
            x = -getB() / (2*getA());

			Res = x;
		}else {
            Res = 0;
        }

		return Res;
	}

	public String toString() {
		return ("Duskruminant = " + calculateDuskruminant()+ ", \nResult = "+calculateResult());

	}



}
