public class Duskruminant {

    private int zminnaA;
    private int zminnaB;
    private int zminnaC;

    public Duskruminant(int a, int b, int c){

        this.zminnaA = a;
        this.zminnaB = b;
        this.zminnaC = c;

    }

    public float calculateDuskruminant(){

        return (zminnaB*zminnaB) - 4*zminnaA*zminnaC;
    }

    public float getA(){

        return zminnaA;
    }
    public float getB(){

        return zminnaB;
    }

    public float getC(){

        return zminnaC;
    }




//        float d = (b*b) - 4*a*c;

//        System.out.println("Duskruminant = "+d);

//        if (d >0){
//            double x1, x2;
//            x1 = (-b + Math.sqrt(d))/(2*a);
//            x2 = (-b - Math.sqrt(d))/(2*a);
//
//            System.out.println("x1 = " + x1 + ", \nx2 = "+x2);
//        }else if (d == 0){
//            double x;
//            x = -b / (2*a);
//
//            System.out.println("x = "+x);
//        }else {
//            System.out.println("No solutions!!");
//        }





}
