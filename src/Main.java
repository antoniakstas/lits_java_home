import java.util.Scanner;

public class Main {
    public static class Person{
        private int personId;
        public Person(int personId){
            this.personId = personId;
        }
    }

    public static void main(String[] args) {

        System.out.println("Calculator of square equations:");


        Scanner in = new Scanner(System.in);

        System.out.print("Enter a:");
        int a = in.nextInt();
        System.out.print("Enter b:");
        int b = in.nextInt();
        System.out.print("Enter c:");
        int c = in.nextInt();

        Result D = new Result(a,b,c);
        System.out.println(D);

        Person s1 = new Person(001);
        Person s2 = new Person(002);
        int h1, h2;
        h1 = s1.hashCode();
        h2 = s2.hashCode();

        if (s1.equals(s2))
            System.out.println("hello");
        else
            System.out.println("bye");

        if (h1>h2)
            System.out.println("000");
        else
            System.out.println("111");

        System.out.println("Enter the size of the array: ");
        int num = in.nextInt();

        int array[], numberToFind, i;

        array = new int[num];

        System.out.println("Enter "+num+" numbers");

        for ( i = 0; i<num; i++){
            array[i] = in.nextInt();
        }

        System.out.println("Enter the number to find: ");
        numberToFind = in.nextInt();

        for (i = 0; i<num; i++){
            if (array[i]==numberToFind){
                System.out.println("The number "+numberToFind+" is at number "+(i+1)+" in the array");
                break;
            }
        }
        if (i==num){
            System.out.println("Number "+numberToFind+" is not found in the array");
        }


    }
}
